const express = require('express');
const path = require('path');
const app = express();
const router = require('./routers/router');
const bodyparser = require('body-parser');

app.use(bodyparser.json());
app.use('', router);
app.use(express.static(path.join(__dirname, './')));
const port = process.env.PORT || 5000;
app.listen({port: port});
//we serve GZIP version of our website
router.get('*.js', function (req, res) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'text/javascript');
    console.log(req.url);
    res.sendFile(path.join(__dirname, req.url));
});
console.log('app on ' + port);
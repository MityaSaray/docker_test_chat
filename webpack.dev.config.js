const webpack = require('webpack');
const path = require('path');
const config = {
    entry: ['babel-polyfill',
        './src/index.js'],
    output: {
        filename: './release/bundle.js',
        path: path.resolve(__dirname, ''),
        publicPath: '/'
    },
    module: {
        rules: [{test: /\.css$/, loader: 'style-loader!css-loader'},
            {
                test: /\.jsx?$/, exclude: path.resolve(__dirname, "node_modules"),
                use: [{loader: "babel-loader", options: {presets: ["es2015", 'react', 'stage-2']}}]
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader',
            },
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'WEBPACK_KEY': JSON.stringify(process.env.WEBPACK_KEY || '')
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jquery: "jquery",
            "window.jQuery": "jquery",
            jQuery: "jquery",
            Popper: ['popper.js', 'default'],
        }),
    ]


};
module.exports = config;

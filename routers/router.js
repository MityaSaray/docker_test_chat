const express = require('express');
const sqDb = require('sqlite3').verbose();
const db = new sqDb.Database(':memory:');

//we create our table
db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS ChatHistory (Name text, Message text, Date text)');
    console.log('db created');
});
//set our changes and connections array
let changes = [];
let connections = [];
let router = express.Router();
router.get('/history', (req, res) => {
    if (req.query.isNew === '0') {
        //if connection not new we add it to connections array;
        connections.push(res);
        function resolve() {
            if (!res.headersSent) {
                //if we have changes we send response to every connection we saved in our array, then set changes and connections to empty arrays.
                if (changes.length > 0) {
                    connections.forEach((res) => {
                        {
                            res.type('json');
                            res.send(changes);
                            res.end()
                        }
                    });
                    connections = [];
                    changes = []
                }
                else {
                //if no changes were made we send response and remove connection from our array
                    res.sendStatus(204);
                    res.end();
                    connections.shift()
                }
            }
        }
        //once a second we check if changes were made and resolve it.
        setInterval(() => {
            resolve()
        }, 1000);
    }
    else {
        //when connection is new we send full chat history
        if (req.query.isNew === '1') {
            db.serialize(() => {
                db.all('SELECT name, message, date FROM ChatHistory', (err, row) => {
                    let dataSend = JSON.stringify(row);
                    res.type('json');
                    res.send(dataSend);
                })
            })
        }
        //we accept only isNew=0 or 1, else we send an error message with text.
        else {
            res.status(400).send(JSON.stringify('Incorrect query, check API'))
        }
    }
});
router.post('/push', (req, res) => {
    let data = req.body;
    //if Name and Date exist we handle request
    if (data.Name && data.Date) {
        //we test if Name holds only letters and numbers and handle error message if not.
        let filter = /^[a-zA-z0-9]+$/;
        if (!filter.test(data.Name)) {
            res.status(400).type('json');
            if (data.Message.length < 200) {
                res.send(JSON.stringify('Make sure your username contains only letters and numbers'))
            }
            //if both username and message aren`t valid.
            else {
                res.send(JSON.stringify('Make sure your username contains only letters and numbers and your message is shorter than 200 symbols'))
            }
        }
        //if everything is fine we add data to database and add changes to our changes array, at the end we respond with 201.
        else if (data.Message.length < 200) {
            db.serialize(() => {
                db.run('INSERT INTO ChatHistory (name, message, date) Values ($name, $message, $date)', {
                    $name: data.Name,
                    $message: data.Message,
                    $date: data.Date
                });
                changes.push({
                    Name: data.Name,
                    Message: data.Message,
                    Date: data.Date
                });
            });
            res.sendStatus(201);
        }
        else {
            res.status(400).type('json').send(JSON.stringify("Make sure your message is shorter than 200 symbols"))
        }
    }
    else {res.status(400).type('json').send(JSON.stringify("make sure username and date "))}
    res.end();
});
module.exports = router;

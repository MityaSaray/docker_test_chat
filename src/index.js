import {render} from 'react-dom'
import React from 'react'
import App from './views/app.js'
import $ from 'jquery';
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import '../node_modules/bootstrap/dist/js/bootstrap.js'
import '../css/main.css'

render(<App/>, document.getElementById("content"));
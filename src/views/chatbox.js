import React from 'react'
import axios from 'axios'
import post from '../logic/pushMessage'

class Chatbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: '', input: "", isNew: 1};
        this.longpoll.bind(this)
    }
    // our handler functions
    longpoll() {
        axios.get(window.location + 'history', {
            timeout: 30000, params: {isNew: this.state.isNew},
        })
            //if request was new we setState with chat history
            .then(res => {
                if (this.state.isNew) {
                    this.setState({data: res.data, isNew: 0});
                }
                else {
                    if (res.status === 200) {
                        let data = [...this.state.data];
                        let result = data.concat(res.data);
                        this.setState({data: result});
                    }
                }
                this.longpoll()
            })
            // if network error we set next request to be new to sync with lost data
            .catch(() => {
                this.setState({isNew: 1});
                this.longpoll()
            });
    }
    submit() {
        let date = new Date();
        post(this.state.user, this.state.input, date, this.state.isNew);
        this.setState({input: ""})
    }

    message(value) {
        this.setState({input: value})
    }

    username(value) {
        this.setState({user: value})
    }
    scrollBot (){
        if (this.state.data.length>0){
            this.bottomContainer.scrollIntoView({behavior: 'smooth'})}
    }
    componentDidMount() {
        console.log('componentdidmount');
        this.longpoll()
    }
    componentDidUpdate() {
        this.scrollBot()
    }

    render() {
        if ((typeof this.state.data) !== 'undefined') {
            return <div>
                <div className={'bg-light text-dark'}>
                    <div className={'w-100 maxheight minheight'}>{this.state.data.map((el, i) => {
                        return <div key={i}><p>{el.Date + "(" + el.Name + "): "}{el.Message}</p>
                            <div className={'bottom'} ref={(div) => {
                                this.bottomContainer = div
                            }}></div>
                        </div>
                    })}
                    </div>
                </div>
                <div className={'mt-5 row justify-content-center'} onKeyPress={(ev) => {
                    if (ev.key === 'Enter') {
                        this.submit()
                    }
                }}><input type={'text'} placeholder={'Enter your nickname'} onChange={(ev) => {
                    this.username(ev.target.value)
                }}/>
                    <input type={'text'} placeholder={'Message max 200 characters'} className={'w-75'}
                           onChange={(event) => {
                               this.message(event.target.value)
                           }} value={this.state.input}/>
                    <button type={'button'} className={'btn btn-light'} onClick={() => {
                        this.submit()
                    }}>Send
                    </button>
                </div>
            </div>
        }
        else {
            return <div className={'text-light'}>Loading</div>
        }
    }
}

export default Chatbox;
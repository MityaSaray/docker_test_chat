import React from 'react'
import Chatbox from './chatbox'
class App extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return <div className={'row h-100 justify-content-center'}>
            <div className={'container col-12 bg-dark'}>
                <div className={'jumbotron jumbotron-fluid text-center'}><h1>Test Chat</h1></div>
                <div className={'container col-8 justify-content-center'}><Chatbox/></div>
            </div>
        </div>
    }

}

export default App;
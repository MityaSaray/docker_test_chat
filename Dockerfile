FROM node:carbon

WORKDIR /tmp
COPY package*.json /tmp/
RUN npm install

WORKDIR /src/app
COPY . /src/app
RUN cp -a /tmp/node_modules /src/app
RUN npm run prod

EXPOSE 5000
CMD ["npm", "start"]

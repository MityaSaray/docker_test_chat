const merge = require('webpack-merge');
const common = require('./webpack.dev.config');
const CompressionPlugin = require('compression-webpack-plugin');
const webpack = require('webpack');
module.exports = merge(common,
    {
        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                }
            }),
            new webpack.optimize.AggressiveMergingPlugin(),
            new CompressionPlugin({
                test: /\.js/,
                asset: '[path].gz[query]',
                algorithm: 'gzip',
                threshold: 10240,
                minRatio: 0.8,
                deleteOriginalAssets: true
            })
        ],
    });
